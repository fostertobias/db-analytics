package com.db.demomidtier;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class Utils {
    private DBConnector dbConnector;
    private Properties properties;

    public Utils() {
        dbConnector = DBConnector.getConnector();
        Connection c = getConnection("dbConnector.properties");
        dbConnector.connect(c);
    }

    public Utils(Connection connection) {
        dbConnector = DBConnector.getConnector();
        dbConnector.connect(connection);
        usePropertyFile("dbConnector.properties");
    }

    public Connection getConnection(String path) {
        usePropertyFile(path);
        String dbDriver = properties.getProperty("dbDriver");
        String dbPath = properties.getProperty("dbPath");
        String dbName = properties.getProperty("dbName");
        String dbUser = properties.getProperty("dbUser");
        String dbPwd = properties.getProperty("dbPwd");
        try {
            Class.forName(dbDriver);
            return DriverManager.getConnection(dbPath + dbName, dbUser, dbPwd);
        } catch (ClassNotFoundException|SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void usePropertyFile(String path) {
        PropertyLoader pLoader = PropertyLoader.getLoader();
        properties = pLoader.getPropValues(path);
    }

    public String getDBAddress() {
        return properties.getProperty("dbPath");
    }

    public boolean isConnected() {
        return dbConnector.isConnected();
    }

    public JSONArray executeSQL(String query, String... params) throws SQLException {
        if (!dbConnector.isConnected()) {
            return null;
        }
        PreparedStatement ps = dbConnector.getConnection().prepareStatement(query);
        for (int i = 0; i < params.length; i++) {
            ps.setString(i + 1, params[i]);
        }
        try (ResultSet rs = ps.executeQuery()) {
            return convertResultSetToJsonArray(rs);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static JSONArray convertResultSetToJsonArray(ResultSet resultSet) throws SQLException {
        if (resultSet == null) {
            return null;
        }
        JSONArray jsonArray = new JSONArray();
        int total_rows = resultSet.getMetaData().getColumnCount();
        while (resultSet.next()) {
            JSONObject obj = new JSONObject();
            for (int i = 0; i < total_rows; i++) {
                obj.put(resultSet.getMetaData().getColumnLabel(i + 1).toLowerCase(), resultSet.getObject(i + 1));
            }
            jsonArray.put(obj);
        }
        return jsonArray;
    }

    public static String convertResultSetToJsonString(ResultSet resultSet) throws Exception {
        if (resultSet == null) {
            return "null";
        }
        return convertResultSetToJsonArray(resultSet).toString();
    }

    public String getViewName() {
        return properties.getProperty("dbName") + "." + properties.getProperty("viewName");
    }
}